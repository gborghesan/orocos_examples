from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

import os

def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument('component_name', default_value='simple'),
        DeclareLaunchArgument('oro_arch', default_value='-{OROCOS_TARGET}'.format_map(os.environ)),
        DeclareLaunchArgument('connect_to_topics', default_value='True'),

        Node(
            package='orocos_examples',
            executable=['simple_ros_rtt', LaunchConfiguration('oro_arch')],
            name=LaunchConfiguration('component_name'),
            arguments=['--name', LaunchConfiguration('component_name')],
            parameters=[{'connect_to_topics': LaunchConfiguration('connect_to_topics')}],
            output='screen',
            emulate_tty=True
        )
    ])

if __name__ == '__main__':
    from launch import LaunchService
    ls = LaunchService()
    ls.include_launch_description(generate_launch_description())
    ls.run()
