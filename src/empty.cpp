/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include "orocos_examples/empty.hpp"

namespace orocos_examples
{
Empty::Empty(const std::string& name) : TaskContext(name)
{
    std::cout << "Empty [" << getName() << "]: constructed" << std::endl;
}

Empty::~Empty()
{
    std::cout << "Empty [" << getName() << "]: destructed" << std::endl;
}

bool Empty::configureHook()
{
    std::cout << "Empty [" << getName() << "]: configured" << std::endl;
    return true;
}

bool Empty::startHook()
{
    std::cout << "Empty [" << getName() << "]: started" << std::endl;
    return true;
}

void Empty::updateHook()
{
    std::cout << "Empty [" << getName() << "]: updated" << std::endl;
}

void Empty::stopHook()
{
    std::cout << "Empty [" << getName() << "]: stopped" << std::endl;
}

void Empty::cleanupHook()
{
    std::cout << "Empty [" << getName() << "]: cleaned" << std::endl;
}

void Empty::errorHook()
{
    std::cout << "Empty [" << getName() << "]: errored and recovered" << std::endl;
    recover();
}
}  // namespace orocos_examples

ORO_CREATE_COMPONENT(orocos_examples::Empty)
