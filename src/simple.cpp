/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include "orocos_examples/simple.hpp"

namespace orocos_examples
{
Simple::Simple(const std::string& name)
  : TaskContext(name), property_(0), input_port_value_(0), event_port_value_(0), event_port_cb_value_(0), op_callback_value_(0), op_caller_("simple_operation_caller")
{
    std::cout << "Simple [" << getName() << "]: constructed" << std::endl;

    this->addProperty("property", property_).doc("Simple Property");
    this->addProperty("input_port_value", input_port_value_).doc("Latest value processed on Simple Input Port");
    this->addProperty("event_port_value", event_port_value_).doc("Latest value processed on Simple Event Port");
    this->addProperty("event_port_cb_value", event_port_cb_value_).doc("Latest value processed on Event Port with Callback");

    this->ports()->addPort("input_port", input_port_).doc("Simple Input Port");
    this->ports()->addPort("output_port", output_port_).doc("Simple Output Port");

    this->ports()->addEventPort("event_port", event_port_).doc("Simple Event Port");
    this->ports()->addEventPort("event_port_cb", event_port_cb_, boost::bind(&Simple::eventPortCallback, this)).doc("Event Port with Callback.");

    this->provides("simple_service")
        ->addOperation("simple_operation", &Simple::op_callback, this, RTT::ClientThread)
        .arg("input", "An integer input.")
        .doc("Simple Operation");
    this->requires()->addOperationCaller(op_caller_);
}

Simple::~Simple()
{
    std::cout << "Simple [" << getName() << "]: destructed" << std::endl;
}

bool Simple::configureHook()
{
    std::cout << "Simple [" << getName() << "]: configured" << std::endl;
    return true;
}

bool Simple::startHook()
{
    std::cout << "Simple [" << getName() << "]: started" << std::endl;
    return true;
}

void Simple::updateHook()
{
    std::cout << "Simple [" << getName() << "]: updated" << std::endl;

    // Process Inputs
    int input_temp;
    if (input_port_.read(input_temp) == RTT::FlowStatus::NewData)
    {
        input_port_value_ = input_temp;
    }

    int event_temp;
    if (event_port_.read(event_temp) == RTT::FlowStatus::NewData)
    {
        event_port_value_ = event_temp;
    }

    // Do Something
    int calculated_sum = property_ + input_port_value_ + event_port_value_ + event_port_cb_value_;

    if (op_caller_.ready())
    {
        if (op_caller_(calculated_sum))
        {
            std::cout << "Simple [" << getName() << "]: simple_operation_caller called successfully with argument = " << calculated_sum << std::endl;
        }
        else
        {
            std::cerr << "Simple [" << getName() << "]: simple_operation_caller failed with argument = " << calculated_sum << std::endl;
        }
    }
    else
    {
        std::cerr << "Simple [" << getName() << "]: simple_operation_caller not ready to be called" << std::endl;
    }

    // Process Outputs
    if (not output_port_.write(calculated_sum) == RTT::WriteStatus::WriteSuccess)
    {
        std::cerr << "Simple [" << getName() << "]: output_port failed to write" << std::endl;
    }
}

void Simple::stopHook()
{
    std::cout << "Simple [" << getName() << "]: stopped" << std::endl;
}

void Simple::cleanupHook()
{
    std::cout << "Simple [" << getName() << "]: cleaned" << std::endl;
}

void Simple::eventPortCallback()
{
    event_port_cb_.readNewest(event_port_cb_value_);
}

bool Simple::op_callback(int input)
{
    std::cout << "Simple [" << getName() << "]: simple_operation executed successfully with argument = " << input << std::endl;
    return true;
}

}  // namespace orocos_examples

ORO_CREATE_COMPONENT(orocos_examples::Simple)
