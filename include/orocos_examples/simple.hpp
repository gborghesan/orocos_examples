/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#ifndef OROCOS_EXAMPLES_SIMPLE_HPP
#define OROCOS_EXAMPLES_SIMPLE_HPP

#include <rtt/Component.hpp>
#include <rtt/RTT.hpp>

namespace orocos_examples
{
class Simple : public RTT::TaskContext
{
  public:
    explicit Simple(const std::string& name);
    virtual ~Simple();
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    int property_;
    int input_port_value_;
    int event_port_value_;
    int event_port_cb_value_;
    int op_callback_value_;

    RTT::InputPort<int> input_port_;
    RTT::OutputPort<int> output_port_;

    RTT::InputPort<int> event_port_;
    RTT::InputPort<int> event_port_cb_;
    void eventPortCallback();

    RTT::OperationCaller<bool(int)> op_caller_;
    bool op_callback(int input);
};
}  // namespace orocos_examples

#endif
