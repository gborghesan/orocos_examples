/*
 * Copyright (c) 2019 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include <gtest/gtest.h>
#include <rtt/os/startstop.h>
#include <rtt/RTT.hpp>
#include <ocl/DeploymentComponent.hpp>

bool mock_fn(int input)
{
    (void)input;
    return true;
}

class SimpleTest : public testing::Test
{
  public:
    void SetUp()
    {
        deployer.reset(new OCL::DeploymentComponent("Deployer"));

        ASSERT_TRUE(deployer->import("orocos_examples"));
        ASSERT_TRUE(deployer->loadComponent("test_simple", "orocos_examples::Simple"));

        test_simple = deployer->getPeer("test_simple");
        ASSERT_TRUE(test_simple);
    }

    void TearDown()
    {
        deployer->kickOutAll();
        deployer->shutdownDeployment();
        deployer.reset();
    }

    std::shared_ptr<OCL::DeploymentComponent> deployer;
    RTT::TaskContext* test_simple;
};

TEST_F(SimpleTest, Interfaces)
{
    EXPECT_TRUE(test_simple->configure());
    EXPECT_TRUE(test_simple->start());

    // Create some test interaction objects

    RTT::Property<int> prop_property = test_simple->getProperty("property");
    ASSERT_TRUE(prop_property.ready());
    RTT::Property<int> prop_input_port_value = test_simple->getProperty("input_port_value");
    ASSERT_TRUE(prop_input_port_value.ready());
    RTT::Property<int> prop_event_port_value = test_simple->getProperty("event_port_value");
    ASSERT_TRUE(prop_event_port_value.ready());
    RTT::Property<int> prop_event_port_cb_value = test_simple->getProperty("event_port_cb_value");
    ASSERT_TRUE(prop_event_port_cb_value.ready());

    RTT::ConnPolicy input_port_policy = RTT::ConnPolicy::data();
    RTT::OutputPort<int> connect_to_input_port;
    ASSERT_TRUE(connect_to_input_port.connectTo(test_simple->ports()->getPort("input_port"), input_port_policy));

    int output_port_value = 0;
    RTT::ConnPolicy output_port_policy = RTT::ConnPolicy::data();
    RTT::InputPort<int> connect_to_output_port;
    ASSERT_TRUE(test_simple->ports()->getPort("output_port")->connectTo(&connect_to_output_port, output_port_policy));

    RTT::ConnPolicy event_port_policy = RTT::ConnPolicy::data();
    RTT::OutputPort<int> connect_to_event_port;
    ASSERT_TRUE(connect_to_event_port.connectTo(test_simple->ports()->getPort("event_port"), event_port_policy));

    RTT::ConnPolicy event_port_cb_policy = RTT::ConnPolicy::data();
    RTT::OutputPort<int> connect_to_event_port_cb;
    ASSERT_TRUE(connect_to_event_port_cb.connectTo(test_simple->ports()->getPort("event_port_cb"), event_port_cb_policy));

    RTT::OperationCaller<bool(int)> op_simple_operation;
    op_simple_operation = test_simple->provides("simple_service")->getOperation("simple_operation");
    ASSERT_TRUE(op_simple_operation.ready());

    deployer->addOperation("mock", mock_fn, RTT::ClientThread);
    ASSERT_TRUE(deployer->connectOperations("test_simple.simple_operation_caller", "Deployer.mock"));

    // Run some actual tests.

    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(0, prop_property.get());
    EXPECT_EQ(0, prop_input_port_value.get());
    EXPECT_EQ(0, prop_event_port_value.get());
    EXPECT_EQ(0, prop_event_port_cb_value.get());
    EXPECT_EQ(0, output_port_value);

    prop_property.set(1);
    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(1, prop_property.get());
    EXPECT_EQ(0, prop_input_port_value.get());
    EXPECT_EQ(0, prop_event_port_value.get());
    EXPECT_EQ(0, prop_event_port_cb_value.get());
    EXPECT_EQ(0, output_port_value);

    connect_to_input_port.write(2);
    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(1, prop_property.get());
    EXPECT_EQ(0, prop_input_port_value.get());
    EXPECT_EQ(0, prop_event_port_value.get());
    EXPECT_EQ(0, prop_event_port_cb_value.get());
    EXPECT_EQ(0, output_port_value);

    connect_to_event_port.write(3);
    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(1, prop_property.get());
    EXPECT_EQ(2, prop_input_port_value.get());
    EXPECT_EQ(3, prop_event_port_value.get());
    EXPECT_EQ(0, prop_event_port_cb_value.get());
    EXPECT_EQ(6, output_port_value);

    connect_to_event_port_cb.write(4);
    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(1, prop_property.get());
    EXPECT_EQ(2, prop_input_port_value.get());
    EXPECT_EQ(3, prop_event_port_value.get());
    EXPECT_EQ(4, prop_event_port_cb_value.get());
    EXPECT_EQ(6, output_port_value);

    op_simple_operation(5);
    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(1, prop_property.get());
    EXPECT_EQ(2, prop_input_port_value.get());
    EXPECT_EQ(3, prop_event_port_value.get());
    EXPECT_EQ(4, prop_event_port_cb_value.get());
    EXPECT_EQ(6, output_port_value);

    test_simple->setPeriod(1);
    sleep(3.1);  // so component thread can run
    test_simple->setPeriod(0);
    sleep(1);
    connect_to_output_port.read(output_port_value);
    EXPECT_EQ(1, prop_property.get());
    EXPECT_EQ(2, prop_input_port_value.get());
    EXPECT_EQ(3, prop_event_port_value.get());
    EXPECT_EQ(4, prop_event_port_cb_value.get());
    EXPECT_EQ(10, output_port_value);

    EXPECT_TRUE(test_simple->stop());
    EXPECT_TRUE(test_simple->cleanup());
}

int main(int argc, char** argv)
{
    __os_init(argc, argv);
    ::testing::InitGoogleTest(&argc, argv);

    int ret = RUN_ALL_TESTS();

    return ret;
}
